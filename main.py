import pprint

import redis
from flask import Flask, request, jsonify
from rq import Queue

from tasks import upload_file, print_about

app = Flask(__name__)

r = redis.Redis(host='redis', port=6379, db=0)
q = Queue(connection=r)

pp = pprint.PrettyPrinter(indent=4)


@app.route('/upload/<folder_id>/', methods=['GET'])
def upload_photo(folder_id):
    """Загрузка файлов в папку на Google диске"""
    json_data = request.get_json()
	# Ставим задачу в очередь на выполнение
    job = q.enqueue(upload_file, folder_id, json_data)
	
	# Сообщаем, что задача поставлена на выполнение
    return jsonify({'status': 'ok',
                    'detail': f'Задача {job.id} поставлена на выполнение'})


@app.route('/info/', methods=['GET'])
def get_info():
    print_about()
    return jsonify({'ok': 'ok'})