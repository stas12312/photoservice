FROM python:3.6-alpine

RUN adduser -D stas12312
WORKDIR /home/stas12312/src/

COPY requirements.txt requirements.txt
RUN pip install -r requirements.txt

COPY ./src ./
RUN chown -R stas12312:stas12312 ./
USER stas12312
CMD gunicorn app --host 127.0.0.1 --port 5057 --reload app:app