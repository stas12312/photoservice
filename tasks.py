import json
import pprint

import requests
from google.oauth2 import service_account
from googleapiclient.discovery import build
from googleapiclient.http import MediaIoBaseUpload

from utils import download_file_by_url

SCOPES = ['https://www.googleapis.com/auth/drive']
SERVICE_ACCOUNT_FILE = 'auth.json'

credentials = service_account.Credentials.from_service_account_file(
    SERVICE_ACCOUNT_FILE, scopes=SCOPES)
service = build('drive', 'v3', credentials=credentials)

pp = pprint.PrettyPrinter(indent=4)


# ...

def print_about(service_=service):
    """Print information about the user along with the Drive API settings.

    Args:
      service_: Drive API service instance.
    """
    try:
        about = service_.about().get(fields='*').execute()
        pp.pprint(about)
    except Exception as error:
        print('An error occurred: %s' % error)


def get_files_in_folder(folder_id):
    """
    Получение фалов в директории с folder_id
    """
    page_token = None
    all_files = []
    while True:
        param = {}
        if page_token:
            param['pageToken'] = page_token
        result = service.files().list(
            pageSize=20, fields="nextPageToken, files(id, name, mimeType, parents)",
            q=f"'{folder_id}' in parents", **param).execute()
        all_files += result['files']
        page_token = result.get('nextPageToken')
        if not page_token:
            break
    return all_files


def check_file(files, name):
    """
    Проверка существования файла с именем name
    """
    for file in files:
        if file['name'] == name:
            return file
    return None


def check_file_by_id(files, id):
    """ Проверка существования файла с id"""
    for file in files:
        if int(file['name'].split('.')[0]) == int(id):
            return file
    return None


def create_folder(name, parent_id):
    """
    Создание папки с именем name в папке с parent_id
    """
    file_metadata = {
        'name': name,
        'mimeType': 'application/vnd.google-apps.folder',
        'parents': [parent_id],
    }
    # Возвращаем созданную папку
    return service.files().create(body=file_metadata, fields='id, parents').execute()


def upload_file(folder_id, data):
    # Получем корневые папки для фото
    root_folders = get_files_in_folder(folder_id)
    branch_folder = None
    branch = ''
    date = ''
    # Получаем филиал и проверяем, есть ли папка для этого филиала
    if 'branch' in data:
        branch = data['branch']
        if 'id' in data:
            id_ = f'{data["id"]}. '
            branch_folder = check_file_by_id(root_folders, data['id'])
        else:
            id_ = ''
            branch_folder = check_file(root_folders, branch)
        # Создаем папку для филиала, если её нет
        if not branch_folder:
            branch_folder = create_folder(f'{id_}{branch}', folder_id)

    date_folder = None
    # Получаем дату и проверяем, есть ли папка с этой датой
    if 'date' in data:
        date = data['date']
        date_folder = check_file(get_files_in_folder(branch_folder['id']), date)
        # Создаем папку с датой, если её нет
        if not date_folder:
            date_folder = create_folder(date, branch_folder['id'])

    # Получаем ФИО преподавателя, если он передан
    teacher = ''
    if 'teacher' in data:
        teacher = data['teacher']

    # Получаем ссылки на изображения, которые нужно загрузить
    if 'images' in data:
        images = data['images']

    for image in images:
        ext = image.split('.')[-1]  # Расширение файла
        file_name = f'{teacher}.{ext}'  # Формируем название файла
        file = download_file_by_url(image)
        file_metadata = {
            'name': file_name,
            'parents': [date_folder['id']]
        }
        media_body = MediaIoBaseUpload(file, mimetype='image/jpeg', resumable=True)

        service.files().create(body=file_metadata, media_body=media_body, fields='id, parents, name').execute()

    # Если есть url для оповещения
    if 'callback' in data:
        google_url = 'https://drive.google.com/drive/folders/'
        url = google_url + date_folder['id']
        service_data = {
            'status': 'ok',
            'message': f'Фото успешно загружены на Google диск {url}',
            'url': url,
            'branch': branch,
            'date': date,
        }
        requests.post(data['callback'], data=json.dumps(service_data))
