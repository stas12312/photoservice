from requests import get
import io


def download_file_by_url(url):
    """
    Загрузка файлов по url

    Возвращает файловый объект
    """
    file = io.BytesIO()
    # Получаем изображение
    response = get(url)
    # Записываем в файловый объект
    file.write(response.content)
    return file
